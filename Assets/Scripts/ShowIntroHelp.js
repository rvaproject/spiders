﻿#pragma strict

function Start () {
	
	// The Intro-Help last for 8 seconds
	// Giving time to the User to read the information
	yield WaitForSeconds(8);
	
	// Load next Scene (GAME)
	Application.LoadLevel("CatchTheSpiders");
	
}

function Update () {

}