﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {

	public Transform[] spawn;
    
	void OnTriggerEnter(Collider other) {
		int i = 0;

		if(other.tag == "Spider")	{
			i = Random.Range(0,3);
			other.transform.position = spawn[i].position;
		} else if (other.tag == "Donuts" ) {
			i = Random.Range(4,6);
			other.transform.position = spawn[i].position;	
		} else if(other.tag == "GameOver")	{
			other.transform.position = spawn[7].position;
		}
    }
}