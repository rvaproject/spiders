using UnityEngine;
using System.Collections;

public class Move_Box : MonoBehaviour {

	// Box Movement Speed
	public float speed = 10.0F;
	
	// Update is called once per frame
	void Update () {
		transform.position += new Vector3(Input.GetAxis("Horizontal") * speed,Input.GetAxis("Vertical")* speed,0);
	}

	// POINTS
	public static int counterPoints = 0;

	public static int getPoints () {
		return counterPoints;
	}

	// Spiders number
	public static int counterSpiders = 0;

	public static int getSpiders () {
		return counterSpiders;
	}

	// Donuts number
	public static int counterDonuts = 0;

	public static int getDonuts () {
		return counterDonuts;
	}

	// Store the Best Score
	public static int BestScore = 0; 

	public static int getBestScore () {
		return BestScore;
	}

	// Use this for initialization
	void Start () {
		BestScore = PlayerPrefs.GetInt("BestScore");

		// Change Score points
		TextMesh textObject4 = GameObject.Find("SCORE_BEST").GetComponent<TextMesh>();
		textObject4.text = "BEST SCORE = " + BestScore + " pts";
	}

	// Spawn Position
	public Transform[] spawn;

	// On Collision
	void OnTriggerEnter(Collider coll)
	{
		if (coll.name == "DONUTS")
		{
			counterPoints += 20;			// Each Donut = 20 points

			// Increment Spiders number
			counterDonuts++;

			// Debug
			Debug.Log (">>> Donuts = 20 pts");
			Debug.Log (">>> SCORE = " + counterPoints + " pts");

			// On collision destroy Donut
			//Destroy(coll);
			
		} else if (coll.name == "SPIDER_DROP")
		{
			//GameObject Spider;
			//Spider.GetComponent<Animation>().Play();

			counterPoints += 10;			// Each Spider = 10 points

			// Debug
			Debug.Log (">>> Spider = 10 pts");
			Debug.Log (">>> SCORE = " + counterPoints + " pts");

			// Increment Spiders number
			counterSpiders++;

		}
		else if (coll.name == "GAME_OVER")
		{
			Debug.Log (">>> GAME OVER <<<");
			Debug.Log (">>> SCORE = " + counterPoints + " pts");

			// Finish Game
			Destroy(gameObject);

			// >>> Checking the values 
			Debug.Log("BestScoreBefore = " + BestScore);

			if(BestScore < counterPoints) {
				BestScore = counterPoints;

				// Set Best Score
				PlayerPrefs.SetInt("BestScore", (BestScore));

				// >>> Checking the values 
				Debug.Log("PlayedBeforeAfter = " + BestScore);
			}
			
			// Load next Scene SCORE
			Application.LoadLevel("Score_GameOver");
		}

		// Respawn
		int i = Random.Range(0, spawn.Length-1);
		print(i+'\n');
		print(spawn[i].position);
		coll.transform.position = spawn[i].position;

		// Change Score points
		TextMesh textObject = GameObject.Find("SCORE").GetComponent<TextMesh>();
		textObject.text = "SCORE = " + counterPoints + " pts";

		// Change Score points
		TextMesh textObject2 = GameObject.Find("SCORE_SPIDERS_CATCH").GetComponent<TextMesh>();
		textObject2.text = "" + counterSpiders;

		// Change Score points
		TextMesh textObject3 = GameObject.Find("SCORE_DONUTS_CATCH").GetComponent<TextMesh>();
		textObject3.text = "" + counterDonuts;

		
	}
	
}
