﻿#pragma strict

// Store the played before status of the user
var PlayedBefore;
 
function Start () 
{		
	// Get PlayedBefore status
	PlayedBefore = PlayerPrefs.GetInt("PlayedBeforeSave");
 	
 	// Checking the values 
 	Debug.Log("PlayedBefore = " + PlayedBefore);
 
 	// Checks if this is the first time the user is playing
    if(PlayedBefore == 0) 
    {
         // The logo last for 5 seconds
		 yield WaitForSeconds(5);
	
		 // Load next Scene (INTRO_HELP)
		 Application.LoadLevel("Intro_Help");
               
         // Change status to '1' >>> Not first time
         PlayedBefore = 1;
         
         // Set change value
         PlayerPrefs.SetInt("PlayedBeforeSave", (PlayedBefore));

         // Checking the values 
         Debug.Log("PlayedBefore = " + PlayedBefore);
     } 
     else 
     {
         // The logo last for 5 seconds
		 yield WaitForSeconds(5);
	
		 // Load next Scene (GAME) | Jump Intro-Help Scene
		 Application.LoadLevel("CatchTheSpiders");
         
         // Checking the values
         Debug.Log("PlayedBefore = " + PlayedBefore);
     }
}

function Update () 
{

}
