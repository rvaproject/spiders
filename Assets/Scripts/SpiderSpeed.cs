﻿using UnityEngine;
using System.Collections;

public class SpiderSpeed : MonoBehaviour {

	public int speed = 5;

	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody> ();
		rb.velocity = new Vector3(0,-20 * speed,0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
