﻿using UnityEngine;
using System.Collections;

public class RandomPosition : MonoBehaviour {

	// Vector for the positions where the spiders will spawn
	public Vector3[] positions;

	// Create new game object
	public GameObject[] prefabs;
	public GameObject dropObject;

	//Number of Objects
	private int i = 0;

	// Spawn delay
	public float delay = 0.4f;
	
	// Use this for initialization
	void Start () {

		// Repeate 
		InvokeRepeating("Spawn",delay,delay);

	}

	// Spawn is called once per frame
	void Spawn () { 

		// random Number between 0 - max 
		int randomNumber = Random.Range(0,positions.Length);
		
		// Spawn Object in random position
		//dropObject[i].gameObject.transform.position = positions[randomNumber];

		//Instantiate(prefabs[i], positions[randomNumber], Quaternion.identity);
		Instantiate(dropObject, new Vector3(0.0f,500.0f,200.0f), Quaternion.identity);

		if (i == 4) 
		{
			// RESET Object number
			i = 0;
		} else 
		{
			// Increment Object index
			i++;
		}

	}

	/*
	// On Collision
	void OnCollisionEnter(Collision coll)
	{
		// On collision destroy Object
		Destroy(coll.gameObject);
	}
	*/

	// Update is called once per frame
	void Update () {
		
	}
}
