﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	// GET POINTS FROM GAME
	public static int counterPoints = Move_Box.getPoints();
	
	// GET Spiders number
	public static int counterSpiders = Move_Box.getSpiders();

	// GET BestScore number
	public static int BestScore = Move_Box.getBestScore();
	
	void Start () {
	
		// Show Score points
		TextMesh textObject = GameObject.Find("SCORE").GetComponent<TextMesh>();
		textObject.text = "" + counterPoints + " pts";
		
		// Show Score points
		TextMesh textObject2 = GameObject.Find("SCORE_SPIDERS_CATCH").GetComponent<TextMesh>();
		textObject2.text = "" + counterSpiders;

		if (BestScore == 0) 
		{
			BestScore = PlayerPrefs.GetInt ("BestScore");
		}
		
		// Change Score points
		TextMesh textObject4 = GameObject.Find("SCORE_BEST").GetComponent<TextMesh>();
		textObject4.text = BestScore + " pts";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
