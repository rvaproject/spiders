﻿#pragma strict

// SPIDER SPEED
var speed : float = 5.5;

function Start () {
    
    // Spider Current Position
    var pointA = transform.position;
    
    // Spider Target Position
    var dst_walk = Vector3(-130, 50, -150);

    var Spider_walk : GameObject;

    while (true) {
    
    		//Spider_walk = GameObject.Find("SPIDER_WALK");

	    	// MOVE FORWARD
	        yield MoveObject(transform, pointA, dst_walk, speed);

	        // MOVE BACKWARD
	        yield MoveObject(transform, dst_walk, pointA, speed);
	                
    }
}

// MOVE SPIDER BACK AND FORTH
function MoveObject (thisTransform : Transform, startPos : Vector3, endPos : Vector3, time : float) {
    
    var i = 0.0;
    var rate = 1.0/time;
    
    while (i < 1.0) {
        i += Time.deltaTime * rate;
        thisTransform.position = Vector3.Lerp(startPos, endPos, i);
        yield; 
    }
    
    // ROTATE SPIDER degrees
    transform.Rotate(0,180,0);
}